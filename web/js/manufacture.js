$(document).ready(function () {
    loadDataManufacture();
    showModalCreate();
    showModalDetele();
    showModalEdit();
});

function loadDataManufacture() {
    $(".manufacture-list").DataTable({
        searching: false,
        lengthChange: false,
        order: [],
        info: false,
        ajax: { url: "/api/manufacture", dataSrc: "" },
        columns: [
            { "data": "name", "targets": 'no-sort', "orderable": false, },
            { "data": "address", "targets": 'no-sort', "orderable": false },
            {
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    return `<button data-id=${buttonID} name="edit_btn" class="btn btn-primary edit_btn" data-toggle="modal" data-target="#editManufactureModal">Edit</button>
                            <button data-id=${buttonID} name="delete_btn" class="btn btn-danger delete_btn" data-toggle="modal" data-target="#deleteManufactureModal">Delete</button>`;
                },
                "targets": 'no-sort', "orderable": false
            }
        ]
    });
};

function showModalCreate() {
    $('#createManufactureModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text(recipient)
        createManufacture();
    })
}

function showModalDetele() {
    $(document).on("click", ".delete_btn", function () {
        let idManufacture = $(this).closest('tr').find('td .delete_btn').attr("data-id");
        $("#deleteManufactureModal").show();
        deleteManufacture(idManufacture);
    });
}

function createManufacture() {
    $("#createManufactures").click(function () {
        let name = $("#name").val();
        let address = $("#address").val();
        $.ajax({
            type: "POST",
            url: "/api/manufacture",
            data: { "name": name, "address": address },
            success: function () {
                $('#createManufactureModal').modal('hide');
                $('.manufacture-list').DataTable().ajax.reload();
                $("#name").val("");
                $("#address").val("");
            },
            error: function (err) {
                if (err.responseJSON) {
                    getErrorValidate(err.responseJSON)

                } else {
                    alert("ajax error");
                }
            },
        });
    })
}

function deleteManufacture(idManufacture) {
    $(".delete-btn").click(function () {
        $.ajax({
            type: "DELETE",
            url: "/api/manufacture/" + idManufacture,
            success: function () {
                $('#deleteManufactureModal').modal('hide');
                $('.manufacture-list').DataTable().ajax.reload();
            },
            error: function () { alert("ajax error"); },
        });
    })
}

function showModalEdit() {
    $(document).on("click", ".edit_btn", function () {
        let idManufacture = $(this).closest('tr').find('td .edit_btn').attr("data-id");
        $("#manu-edit-id").val(idManufacture);
        $("#editManufactureModal").show();
        showInfoManufacture(idManufacture);
        editManufacture(idManufacture);
    });
}

function showInfoManufacture(idManufacture) {
    $.ajax({
        type: "GET",
        url: "/api/manufacture/" + idManufacture,
        success: function (data) {
            $("#name-edit").val(data.name);
            $("#address-edit").val(data.address);
        },
        error: function () { alert("ajax error"); },
    });
}

function editManufacture(idManufacture) {
    $("#editManufactures").click(function () {
        let name = $("#name-edit").val();
        let address = $("#address-edit").val();
        $.ajax({
            type: "PUT",
            url: "/api/manufacture/" + idManufacture,
            data: { "name": name, "address": address },
            success: function () {
                $('#editManufactureModal').modal('hide');
                $('.manufacture-list').DataTable().ajax.reload();
            },
            error: function (err) {
                if (err.responseJSON) {
                    getErrorValidate(err.responseJSON)

                } else {
                    alert("ajax error");
                }
            },
        });
    })
}

function getErrorValidate(err_json) {
    let error_validate = [];
    $.each(err_json, function (index, value) {
        error_validate.push(value.message);
    });
    alert(error_validate.toString())
}