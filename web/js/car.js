$(document).ready(function () {
    let apiListCar = '/api/car';
    let selectHtml = '#findManufactures';
    loadDataCar(apiListCar);
    loadListManufacture(selectHtml);
    showModalCreateCar();
    showModalDeteleCar();
    showModalEditCar();
});

function loadDataCar(apiListCar) {
    $(".car-list").DataTable({
        searching: false,
        lengthChange: false,
        order: [],
        info: false,
        ajax: { url: apiListCar, dataSrc: "" },
        columns: [
            { "data": "name" },
            { "data": "model" },
            { "data": "manufacture.name" },
            {
                "data": "buy_date", render: function (d) {
                    return moment(d).format('DD-MM-YYYY');
                },
            },
            {
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    return `<button data-id=${buttonID} name="edit_btn" class="btn btn-primary edit_btn" data-toggle="modal" data-target="#editCarModal">Edit</button>
                            <button data-id=${buttonID} name="delete_btn" class="btn btn-danger delete_btn" data-toggle="modal" data-target="#deleteCarModal">Delete</button>`;
                },
                "targets": 'no-sort', "orderable": false
            }
        ]
    });

    searchCar();
};

function loadListManufacture(selectHtml) {
    $.ajax({
        type: "GET",
        url: "/api/manufacture/",
        success: function (data) {
            $(selectHtml).html("");
            $(selectHtml).append($("<option value=''>All manufacture</option>"));
            $.each(data, function (key, value) {
                $(selectHtml).append($("<option></option>").attr("value", value.id).text(value.name));
            });
        },
        error: function () { alert("ajax error"); },
    });
}

function searchCar() {
    $("#searchCar").click(function () {
        let nameCar = $("#findNameCar").val();
        let manufactureId = $('#findManufactures :selected').val();
        let searchManufacture = "";
        let searchNameCar = "";
        if (nameCar != "" && nameCar != undefined) {
            searchNameCar = "?name=" + nameCar;
        }
        if (manufactureId != "" && manufactureId != undefined) {
            searchManufacture = (searchNameCar != "") ? "&manufacture_id=" + manufactureId : "?manufacture_id=" + manufactureId;
        }
        let apiListCar = "/api/car" + searchNameCar + searchManufacture;
        $(".car-list").DataTable().ajax.url(apiListCar).load();
    });
}

function showModalCreateCar() {
    $('#createCarModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text(recipient)
        let selectHtmlModalCreate = '#manufature-id'
        $("#buy-date").datepicker({
            dateFormat: "dd/mm/yy"
        });
        loadListManufacture(selectHtmlModalCreate)
        createCar();
    })
}

function createCar() {
    $("#createCar").click(function () {
        let name = $("#name").val();
        let model = $("#model").val();
        let manufatureId = $("#manufature-id").val();
        let buyDate = $("#buy-date").val();
        let buyDateFormat = moment(buyDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
        $.ajax({
            type: "POST",
            url: "/api/car",
            data: { "name": name, "model": model, "manufacture_id": manufatureId, "buy_date": buyDateFormat },
            success: function () {
                $('#createCarModal').modal('hide');
                $('.car-list').DataTable().ajax.reload();
                $("#name").val("");
                $("#model").val("");
                $("#manufature-id").val("");
                $("#buy-date").val("");
            },
            error: function (err) {
                if (err.responseJSON) {
                    getErrorValidate(err.responseJSON)

                } else {
                    alert("ajax error");
                }
            },
        });
    })
}

function showModalDeteleCar() {
    $(document).on("click", ".delete_btn", function () {
        let idCar = $(this).closest('tr').find('td .delete_btn').attr("data-id");
        $("#deleteCarModal").show();
        deleteCar(idCar);
    });
}

function deleteCar(idCar) {
    $(".delete-btn").click(function () {
        $.ajax({
            type: "DELETE",
            url: "/api/car/" + idCar,
            success: function () {
                $('#deleteCarModal').modal('hide');
                $('.car-list').DataTable().ajax.reload();
            },
            error: function () { alert("ajax error"); },
        });
    })
}

function showModalEditCar() {
    $(document).on("click", ".edit_btn", function () {
        let idCar = $(this).closest('tr').find('td .edit_btn').attr("data-id");
        $("#editCarModal").show();
        $("#buy-date-edit").datepicker({
            dateFormat: "dd/mm/yy"
        });
        showInfoCar(idCar);
        editCar(idCar);
    });
}

function showInfoCar(idCar) {
    let selectHtmlModalEdit = '#manu-id';
    loadListManufacture(selectHtmlModalEdit)
    $.ajax({
        type: "GET",
        url: "/api/car/" + idCar,
        success: function (data) {
            $("#name-car").val(data.name);
            $("#model-car").val(data.model);
            $("#manu-id").val(data.manufacture_id);
            let buy_date = moment(data.buy_date).format('DD/MM/YYYY');
            $("#buy-date-edit").val(buy_date);
        },
        error: function () { alert("ajax error"); },
    });
}

function editCar(idCar) {
    $("#editCars").click(function () {
        let name = $("#name-car").val();
        let model = $("#model-car").val();
        let manufatureId = $("#manu-id").val();
        let buyDate = $("#buy-date-edit").val();
        let buyDateFormat = moment(buyDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
        $.ajax({
            type: "PUT",
            url: "/api/car/" + idCar,
            data: { "name": name, "model": model, "manufacture_id": manufatureId, "buy_date": buyDateFormat },
            success: function () {
                $('#editCarModal').modal('hide');
                $('.car-list').DataTable().ajax.reload();
            },
            error: function (err) {
                if (err.responseJSON) {
                    getErrorValidate(err.responseJSON)

                } else {
                    alert("ajax error");
                }
            },
        });
    })
}

function getErrorValidate(err_json) {
    let error_validate = [];
    $.each(err_json, function (index, value) {
        error_validate.push(value.message);
    });
    alert(error_validate.toString())
}