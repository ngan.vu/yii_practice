<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manufactures".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string|null $create_at
 * @property string|null $updated_at
 */
class Manufactures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manufactures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address'], 'required'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
        ];
    }
}
