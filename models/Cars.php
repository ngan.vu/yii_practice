<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property int $manufacture_id
 * @property string $name
 * @property string $address
 * @property string|null $buy_date
 *
 * @property Manufactures $manufacture
 */
class Cars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manufacture_id', 'name', 'model', 'buy_date'], 'required'],
            [['manufacture_id'], 'integer'],
            [['buy_date'],'datetime', 'format' => 'php:Y-m-d'],
            [['name', 'model'], 'string', 'max' => 255],
            [['manufacture_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manufactures::className(), 'targetAttribute' => ['manufacture_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacture_id' => 'Manufacture ID',
            'name' => 'Name',
            'model' => 'Model',
            'buy_date' => 'Buy Date',
        ];
    }

    /**
     * Gets query for [[Manufacture]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getManufacture()
    {
        return $this->hasOne(Manufactures::className(), ['id' => 'manufacture_id']);
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'model',
            'manufacture' => function ($model) {
                return $model->manufacture;
            },
            'manufacture_id',
            'buy_date'
        ];
    }
}
