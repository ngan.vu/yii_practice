<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */

use app\assets\CarAsset;

CarAsset::register($this);
$this->title = 'Car Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Car-site">
    <div class="search-frame mb-3 form-inline">
        <div class="form-group col-sm-3">
            <input type="text" class="form-control" id="findNameCar" placeholder="Find by car name">
        </div>
        <div class="form-group col-sm-3">
            <select class="form-control" id="findManufactures">
                <option value="">All manufacture</option>
            </select>
        </div>
        <button class="btn btn-primary" id="searchCar">Filter</button>
    </div>
    <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#createCarModal" data-whatever="Add new Car" style="float: right;">Add new car</button>
    <table class="table car-list">
        <thead>
            <tr>
                <th>Name</th>
                <th>Model</th>
                <th>Manufacture</th>
                <th>Buy Date</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<!-- Create Car -->
<div class="modal fade" id="createCarModal" tabindex="-1" role="dialog" aria-labelledby="createCarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createCarModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label col-form-label-sm">Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="model" class="col-sm-3 col-form-label col-form-label-sm">Model:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="model">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="manufature-id" class="col-sm-3 col-form-label col-form-label-sm">Manufacture:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="manufature-id">
                                <option value="">All manufacture</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="buy-date" class="col-sm-3 col-form-label col-form-label-sm">Buy Date:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="buy-date"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary" id="createCar">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Car -->

<div class="modal fade" id="deleteCarModal" tabindex="-1" role="dialog" aria-labelledby="deleteCarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteCarModalLabel">Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this car</p>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary delete-btn" id="deleteCars">Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Car -->
<div class="modal fade" id="editCarModal" tabindex="-1" role="dialog" aria-labelledby="editCarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editCarModalLabel">Car Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="name-car" class="col-sm-3 col-form-label col-form-label-sm">Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="name-car">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="model-car" class="col-sm-3 col-form-label col-form-label-sm">Model:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="model-car">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="manu-id" class="col-sm-3 col-form-label col-form-label-sm">Manufacture:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="manu-id">
                                <option value="">All manufacture</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="buy-date-edit" class="col-sm-3 col-form-label col-form-label-sm">Buy Date:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="buy-date-edit"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary" id="editCars">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>