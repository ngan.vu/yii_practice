<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */

use app\assets\ManufactureAsset;

ManufactureAsset::register($this);

$this->title = 'Manufacture Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacture-site">
    <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#createManufactureModal" data-whatever="Add new manufacture" style="float: right;">Add new manufacture</button>
    <table class="table manufacture-list">
        <thead>
            <tr>
                <th>Manufacture name</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<!-- Create Manufacture -->
<div class="modal fade" id="createManufactureModal" tabindex="-1" role="dialog" aria-labelledby="createManufactureModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createManufactureModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label col-form-label-sm">Address:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control form-control-sm" id="address"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary" id="createManufactures">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Manufacture -->

<div class="modal fade" id="deleteManufactureModal" tabindex="-1" role="dialog" aria-labelledby="deleteManufactureModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteManufactureModalLabel">Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this manufacture</p>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary delete-btn" id="deleteManufactures">Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Manufacture -->
<div class="modal fade" id="editManufactureModal" tabindex="-1" role="dialog" aria-labelledby="editManufactureModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editManufactureModalLabel">Edit Manufacture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="name-edit">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label col-form-label-sm">Address:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control form-control-sm" id="address-edit"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary" id="editManufactures">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>