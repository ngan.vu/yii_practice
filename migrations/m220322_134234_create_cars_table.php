<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cars}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%manufactures}}`
 */
class m220322_134234_create_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cars', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'model' => $this->string(),
            'manufacture_id' => $this->integer()->notNull(),
            'buy_date' => $this->date(),
        ]);

        // creates index for column `manufacture_id`
        $this->createIndex(
            'idx-cars-manufacture_id',
            'cars',
            'manufacture_id'
        );

        // add foreign key for table `{{%manufactures}}`
        $this->addForeignKey(
            'fk-cars-manufacture_id',
            'cars',
            'manufacture_id',
            'manufactures',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%manufactures}}`
        $this->dropForeignKey(
            'fk-cars-manufacture_id',
            'cars'
        );

        // drops index for column `manufacture_id`
        $this->dropIndex(
            'idx-cars-manufacture_id',
            'cars'
        );

        $this->dropTable('cars');
    }
}
