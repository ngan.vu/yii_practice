<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%manufactures}}`.
 */
class m220319_074212_create_manufactures_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('manufactures', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('manufactures');
    }
}
