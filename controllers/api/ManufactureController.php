<?php

namespace app\controllers\api;

use yii\rest\ActiveController;

class ManufactureController extends ActiveController
{
    public $modelClass = 'app\models\Manufactures';
}
