<?php

namespace app\controllers\api;

use app\models\Cars;
use yii\rest\ActiveController;
use Yii;

class CarController extends ActiveController
{
    public $modelClass = 'app\models\Cars';
    public function actions()
    {

        $actions = [
            'index' => [
                'class'       => 'app\controllers\api\SearchAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'params'      => \Yii::$app->request->get()
            ],
        ];

        return array_merge(parent::actions(), $actions);
    }
}
