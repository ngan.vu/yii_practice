<?php

namespace app\assets;

use yii\web\AssetBundle;

class ManufactureAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $js = [
        'js/manufacture.js'
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
