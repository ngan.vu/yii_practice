<?php

namespace app\assets;

use yii\web\AssetBundle;

class CarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $js = [
        'js/car.js'
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
